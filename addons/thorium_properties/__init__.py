bl_info = {
    "name": "Thorium materials",
    "description": "Thorium materials integration",
    "category": "Object",
    "author": "Stepan Tezyunichev",
    "version": (0, 2),
    "blender": (2, 65, 0),
    "wiki_url": "https://bitbucket.org/radiosity/blender/wiki/Home",
    "tracker_url": "https://bitbucket.org/radiosity/blender/issues",
    "category": "Material",
    "support": "TESTING"
}

import bpy
from bpy.types import Menu, Panel, UIList


class ThoriumPhysicalProperties(bpy.types.PropertyGroup):
    temperature = bpy.props.FloatProperty(name="Temperature", description="Object temperature, K", default=300, precision=1)
    shell = bpy.props.BoolProperty(name="Shell", description="Is object shell or solid. Note that shell parameters defined in material properties.", default=True)
    heat_source = bpy.props.BoolProperty(name="Heat source", description="Is object produce or consume energy.", default=False)
    heat_power = bpy.props.FloatProperty(name="Power", description="Power, W", default=10, precision=2)


class ThoriumShellSettings(bpy.types.PropertyGroup):
    density = bpy.props.FloatProperty(name="Density", description="Material density, kg/m^3", precision=2)
    heat_capacity = bpy.props.FloatProperty(name="Heat capacity", description="Material heat capacity, J/(kg*K)",
                                            precision=2)
    thermal_conductivity = bpy.props.FloatProperty(name="Thermal conductivity",
                                                   description="Material thermal conductivity, W/(m*K)", precision=2)
    thickness = bpy.props.FloatProperty(name="Thickness", description="Thickness, m", precision=4)


class ThoriumOpticalSettings(bpy.types.PropertyGroup):
    specular_reflectance = bpy.props.FloatProperty(name="Specular reflectance",
                                                   description="Specular reflectance coefficient", precision=3)
    diffuse_reflectance = bpy.props.FloatProperty(name="Diffuse reflectance",
                                                  description="Diffuse reflectance coefficient", precision=3)
    absorbance = bpy.props.FloatProperty(name="Absorbance",
                                         description="The ratio of the light energy falling on a body to absorbed by it",
                                         precision=3)
    transmittance = bpy.props.FloatProperty(name="Transmittance",
                                            description="The ratio of the light energy falling on a body to  transmitted through it",
                                            precision=3)
    emissivity = bpy.props.FloatProperty(name="Emissivity",
                                         description="Effectiveness in emitting energy as thermal radiation",
                                         precision=3)


class ThoriumMaterialSettings(bpy.types.PropertyGroup):
    shell = bpy.props.PointerProperty(type=ThoriumShellSettings)
    front = bpy.props.PointerProperty(type=ThoriumOpticalSettings)
    rear = bpy.props.PointerProperty(type=ThoriumOpticalSettings)


class MaterialButtonsPanel:
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "material"

    # COMPAT_ENGINES must be defined in each subclass, external engines can add themselves here

    @classmethod
    def poll(cls, context):
        return context.material and (context.scene.render.engine in cls.COMPAT_ENGINES)


def active_node_mat(mat):
    # TODO, 2.4x has a pipeline section, for 2.5 we need to communicate
    # which settings from node-materials are used
    if mat is not None:
        mat_node = mat.active_node_material
        if mat_node:
            return mat_node
        else:
            return mat

    return None


def check_material(mat):
    if mat is not None:
        if mat.use_nodes:
            if mat.active_node_material is not None:
                return True
            return False
        return True
    return False


class MATERIAL_PT_thorium_shell(MaterialButtonsPanel, Panel):
    bl_label = "Thorium Shell"
    COMPAT_ENGINES = {'BLENDER_RENDER'}

    @classmethod
    def poll(cls, context):
        mat = context.material
        engine = context.scene.render.engine
        return check_material(mat) and (mat.type in {'SURFACE', 'WIRE'}) and (engine in cls.COMPAT_ENGINES)

    def draw(self, context):
        layout = self.layout

        mat = active_node_mat(context.material)

        if mat.type in {'SURFACE', 'WIRE'}:
            split = layout.split()
            thorium = mat.thorium
            shell = thorium.shell

            col = split.column()
            col.prop(shell, "density")
            col.prop(shell, "heat_capacity")
            col.prop(shell, "thermal_conductivity")
            col.prop(shell, "thickness")


class MATERIAL_PT_thorium_optical(MaterialButtonsPanel, Panel):
    bl_label = "Thorium Optical"
    COMPAT_ENGINES = {'BLENDER_RENDER'}

    @classmethod
    def poll(cls, context):
        mat = context.material
        engine = context.scene.render.engine
        return check_material(mat) and (mat.type in {'SURFACE', 'WIRE'}) and (engine in cls.COMPAT_ENGINES)

    def draw(self, context):
        layout = self.layout

        mat = active_node_mat(context.material)

        if mat.type in {'SURFACE', 'WIRE'}:
            split = layout.split()
            thorium = mat.thorium
            front = thorium.front
            rear = thorium.rear

            col = split.column()
            col.label("Front")
            self._add_values(col, front)
            col = split.column()
            col.label("Rear")
            self._add_values(col, rear)

    def _add_values(self, col, settings):
        col.prop(settings, "specular_reflectance")
        col.prop(settings, "diffuse_reflectance")
        col.prop(settings, "absorbance")
        col.prop(settings, "transmittance")
        col.prop(settings, "emissivity")


class ObjectButtonsPanel:
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "object"


class PHYSICS_PT_rigidbody_panel:
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "physics"


class PHYSICS_PT_thorium(PHYSICS_PT_rigidbody_panel, Panel):
    bl_label = "Thorium properties"

    @classmethod
    def poll(cls, context):
        obj = context.object
        return (obj and obj.thorium and
                (not context.scene.render.use_game_engine))

    def draw(self, context):
        layout = self.layout

        ob = context.object
        throrium = ob.thorium

        if throrium is not None:
            layout.prop(throrium, "temperature")
            layout.prop(throrium, "shell")

            row = layout.row()
            #col.label(text="Energy source:")
            row.prop(throrium, "heat_source")
            sub = row.column()
            sub.active = throrium.heat_source
            sub.prop(throrium, "heat_power")


BUILTIN_MATERIALS = [
    ("Aluminium (Al) polished", dict(
    shell=dict(
        density=2698.899902,
        heat_capacity=903.,
        thermal_conductivity=237.,
        thickness=0.01,
        ),
    optical=dict(
        specular_reflectance=0.9,
        diffuse_reflectance=0.05,
        absorbance=0.05,
    ))),
    ("Lead (Pb) oxyde", dict(
        shell=dict(
            density=11340.,
            heat_capacity=1130.,
            thermal_conductivity=35.3,
            thickness=0.01,
        ),
        optical=dict(
            specular_reflectance=0.1,
            diffuse_reflectance=0.1,
            absorbance=0.8,
        ))),
                     ]

class CreateBuiltinThoriumMaterials(bpy.types.Operator):
    """Create built-in thorium materials"""
    bl_idname = "object.create_builtin_thorium_materials"

    bl_label = "Create Thorium materials"
    bl_options = { 'REGISTER', 'UNDO' }

    def execute(self, context):
        for name, params in BUILTIN_MATERIALS:
            if name not in bpy.data.materials:
                m = bpy.data.materials.new(name)
                for k, v in params['shell'].items():
                    setattr(m.thorium.shell, k, v)
                for k, v in params['optical'].items():
                    setattr(m.thorium.front, k, v)
                    setattr(m.thorium.rear, k, v)
        return {'FINISHED'}



class CreateThoriumMaterialsPanel(bpy.types.Panel):
    """A custom panel in the viewport toolbar"""
    bl_label = "Create Thorium materials"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'

    def draw(self, context):
        layout = self.layout

        row = layout.row()

        row.label(text="Thorium materials")
        row = layout.row()
        row.operator("object.create_builtin_thorium_materials", text = "Create Thorium materials", icon = 'ROTACTIVE')

def register():
    bpy.utils.register_class(ThoriumShellSettings)
    bpy.utils.register_class(ThoriumOpticalSettings)
    bpy.utils.register_class(ThoriumMaterialSettings)
    bpy.types.Material.thorium = bpy.props.PointerProperty(type=ThoriumMaterialSettings)

    bpy.utils.register_class(ThoriumPhysicalProperties)
    bpy.types.Object.thorium = bpy.props.PointerProperty(type=ThoriumPhysicalProperties)

    # UI
    bpy.utils.register_class(MATERIAL_PT_thorium_shell)
    bpy.utils.register_class(MATERIAL_PT_thorium_optical)
    bpy.utils.register_class(PHYSICS_PT_thorium)

    bpy.utils.register_class(CreateBuiltinThoriumMaterials)
    bpy.utils.register_class(CreateThoriumMaterialsPanel)


def unregister():
    bpy.utils.unregister_class(PHYSICS_PT_thorium)
    bpy.utils.unregister_class(MATERIAL_PT_thorium_optical)
    bpy.utils.unregister_class(MATERIAL_PT_thorium_shell)

    bpy.utils.unregister_class(ThoriumPhysicalProperties)

    bpy.utils.unregister_class(ThoriumMaterialSettings)
    bpy.utils.unregister_class(ThoriumOpticalSettings)
    bpy.utils.unregister_class(ThoriumShellSettings)

    bpy.utils.unregister_class(CreateBuiltinThoriumMaterials)
    bpy.utils.unregister_class(CreateThoriumMaterialsPanel)


if __name__ == "__main__":
    bpy.utils.register_module(__name__)
