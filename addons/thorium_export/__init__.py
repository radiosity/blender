# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

# <pep8-80 compliant>

bl_info = {
    "name": "Thorium OBJ format",
    "author": "Stepan Tezyunichev, Campbell Barton, Bastien Montagne",
    "version": (0, 1, 0),
    "blender": (2, 77, 0),
    "location": "File > Import-Export",
    "description": "Export to Thorium OBJ",
    "warning": "",
    "wiki_url": "https://bitbucket.org/radiosity/blender/wiki/Home",
    "support": 'TESTING',
    "category": "Import-Export"}

if "bpy" in locals():
    import importlib
    if "export_obj" in locals():
        importlib.reload(export_obj)
    if "import_task" in locals():
        importlib.reload(import_task)


import bpy
from bpy.props import (
        BoolProperty,
        FloatProperty,
        StringProperty,
        EnumProperty,
        IntProperty
        )
from bpy_extras.io_utils import (
        ExportHelper,
        ImportHelper,
        orientation_helper_factory,
        path_reference_mode,
        axis_conversion,
        )


IOOBJOrientationHelper = orientation_helper_factory("IOOBJOrientationHelper", axis_forward='Y', axis_up='Z')


class ExportOBJ(bpy.types.Operator, ExportHelper, IOOBJOrientationHelper):
    """Save a Thorium OBJ File"""

    bl_idname = "export_scene.obj"
    bl_label = 'Export OBJ'
    bl_options = {'PRESET'}

    filename_ext = ".obj"
    filter_glob = StringProperty(
            default="*.obj;*.mtl;*.task",
            options={'HIDDEN'},
            )

    # context group
    use_selection = BoolProperty(
            name="Selection Only",
            description="Export selected objects only",
            default=False,
            )
    use_animation = BoolProperty(
            name="Animation",
            description="Write out an OBJ for each frame",
            default=False,
            )

    # object group
    use_mesh_modifiers = BoolProperty(
            name="Apply Modifiers",
            description="Apply modifiers (preview resolution)",
            default=True,
            )
            
    # extra data group
    use_edges = BoolProperty(
            name="Include Edges",
            description="",
            default=True,
            )
    use_smooth_groups = BoolProperty(
            name="Smooth Groups",
            description="Write sharp edges as smooth groups",
            default=False,
            )
    use_smooth_groups_bitflags = BoolProperty(
            name="Bitflag Smooth Groups",
            description="Same as 'Smooth Groups', but generate smooth groups IDs as bitflags "
                        "(produces at most 32 different smooth groups, usually much less)",
            default=False,
            )
    use_normals = BoolProperty(
            name="Write Normals",
            description="Export one normal per vertex and per face, to represent flat faces and sharp edges",
            default=True,
            )
    use_uvs = BoolProperty(
            name="Include UVs",
            description="Write out the active UV coordinates",
            default=True,
            )
    use_materials = BoolProperty(
            name="Write Materials",
            description="Write out the MTL file",
            default=True,
            )
    use_triangles = BoolProperty(
            name="Triangulate Faces",
            description="Convert all faces to triangles",
            default=True,
            )
    use_nurbs = BoolProperty(
            name="Write Nurbs",
            description="Write nurbs curves as OBJ nurbs rather than "
                        "converting to geometry",
            default=False,
            )
    use_vertex_groups = BoolProperty(
            name="Polygroups",
            description="",
            default=False,
            )

    # grouping group
    use_blen_objects = BoolProperty(
            name="Objects as OBJ Objects",
            description="",
            default=True,
            )
    group_by_object = BoolProperty(
            name="Objects as OBJ Groups ",
            description="",
            default=False,
            )
    group_by_material = BoolProperty(
            name="Material Groups",
            description="",
            default=False,
            )
    keep_vertex_order = BoolProperty(
            name="Keep Vertex Order",
            description="",
            default=False,
            )

    global_scale = FloatProperty(
            name="Scale",
            min=0.01, max=1000.0,
            default=1.0,
            )
            
    export_task = BoolProperty(
            name="Export Task",
            description="",
            default=True,
            )
            
    task_time_step = FloatProperty(
            name="Calculation time step",
            description="",
            min=0.01, max=1.0,
            default=0.5,
            )

    frame_time_scale = FloatProperty(
        name="Seconds per frame",
        description="How many seconds in one Blender frame.\nIt is not feasible to make animation for hours at 30fps. So we pretend, for example, that each frame is fice seconds of simulation.",
        min=0.01, max=100.0,
        default=5.0,
    )

    simulation_duration = FloatProperty(
        name="Simulation duration, s",
        description="",
        min=0.01, max=10000.0,
        default=600.0,
    )

    emission_rays_count = IntProperty(
        name="Emission rays count",
        description="Count of rays in body emission radiance simulation",
        min=1, max=10000000,
        default=1000000,
    )

    tle_orbit_filename = StringProperty(
        name="Orbit filename",
        description="TLE orbit description file",
        default="",
    )

    enable_sun_and_earth = BoolProperty(
        name="Enable Sun and Earth",
        description="Enable Sun and Earth reflected/self radiance.",
        default=True,
    )

    solar_rays_count = IntProperty(
        name="Solar rays count",
        description="Count of rays in Solar and Earth reflected radiance simulation",
        min=1, max=10000000,
        default=1000000,
    )

    path_mode = path_reference_mode

    check_extension = True

    def execute(self, context):
        from . import export_obj

        from mathutils import Matrix
        keywords = self.as_keywords(ignore=("axis_forward",
                                            "axis_up",
                                            "global_scale",
                                            "check_existing",
                                            "filter_glob",
                                            ))

        global_matrix = (Matrix.Scale(self.global_scale, 4) *
                         axis_conversion(to_forward=self.axis_forward,
                                         to_up=self.axis_up,
                                         ).to_4x4())

        keywords["global_matrix"] = global_matrix
        return export_obj.save(context, **keywords)


class ImportTask(bpy.types.Operator, ImportHelper):
    """Load a Thorium TASK File"""
    bl_idname = "import_scene.task"
    bl_label = "Import Task"
    bl_options = {'PRESET', 'UNDO'}

    filename_ext = ".task"
    filter_glob = StringProperty(
            default="*.task",
            options={'HIDDEN'},
            )

    animate_color = BoolProperty(
        name="Animate color",
        description="Animate object color as temperature",
        default=True,
    )
    animate_temperature = BoolProperty(
        name="Animate temperatures",
        description="Generate keyframes for objects temperatures",
        default=True,
    )
    use_global_temperatures = BoolProperty(
        name="Global temperatures",
        description="Use global scale for temperatures",
        default=True,
    )
    logarithmic_temperature = BoolProperty(
        name="Logarithmic scale",
        description="Use logarithmic scale for temperatures",
        default=False,
    )
    keyframes_limit = IntProperty(
        name="Keyframes limit",
        description="Amount of keyframes to create",
        default=100,
    )

    def execute(self, context):
        if not self.filepath:
            self.report({'ERROR'}, 'TASK not selected')
            return {'CANCELLED'}

        keywords = self.as_keywords(ignore=("filter_glob",
                                            ))

        from . import import_task
        return import_task.load(context, **keywords)


def menu_func_export(self, context):
    self.layout.operator(ExportOBJ.bl_idname, text="Thorium (.obj)")


def menu_func_import_task(self, context):
    self.layout.operator(ImportTask.bl_idname, text="Thorium task (.task)")


def register():
    bpy.utils.register_module(__name__)

    bpy.types.INFO_MT_file_export.append(menu_func_export)
    bpy.types.INFO_MT_file_import.append(menu_func_import_task)


def unregister():
    bpy.utils.unregister_module(__name__)

    bpy.types.INFO_MT_file_export.remove(menu_func_export)
    bpy.types.INFO_MT_file_import.remove(menu_func_import_task)

if __name__ == "__main__":
    register()
