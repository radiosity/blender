import os

import bpy

import math
from mathutils import Color
from progress_report import ProgressReport


def load(context, filepath, *, animate_color=True, animate_temperature=True, logarithmic_temperature=False,
             keyframes_limit=100, use_global_temperatures=True):
    object_temperatures = {}
    min_temperature = 1000000
    max_temperature = 0
    temperature_transform = logarithmic if logarithmic_temperature else linear
    current_frame = -1
    current_keyframe = -1
    with ProgressReport(context.window_manager) as progress:
        stats = task_stats(filepath)
        progress.enter_substeps(stats.lines_count)
        with open(filepath) as task:
            animate_frame = True  # first frame should be collected
            for line in task.readlines():
                progress.step()
                elements = line.rstrip('\n').split(' ')

                token = elements[0]

                if token == 'tmprt' and animate_frame and len(elements) == 3:
                    name = elements[2]
                    temperature = float(elements[1])
                    min_temperature = min(min_temperature, temperature)
                    max_temperature = max(max_temperature, temperature)
                    object_temperatures[name] = temperature
                elif token == 'newfrm':
                    current_frame += 1  # Different step size is not supported yet
                    next_keyframe = int(current_frame * keyframes_limit / stats.frames_count)
                    if next_keyframe != current_keyframe:
                        animate_frame = True
                        current_keyframe = next_keyframe
                        context.scene.frame_set(current_keyframe)

                    min_temperature = 1000000
                    max_temperature = 0
                    object_temperatures = {}
                elif token == 'earth_position' and len(elements) == 4:
                    name = 'earth'
                    object = context.scene.objects.get(name, None)
                    if object:
                        dim = max(object.dimensions.x, object.dimensions.y, object.dimensions.z) / 2

                        location = [float(x) for x in elements[1:]]
                        distance = math.sqrt(location[0] * location[0] + location[1] * location[1] + location[2] * location[2])
                        scale = dim / 6400000
                        object.location = [x * scale for x in location]
                        object.keyframe_insert(data_path='location')
                        #print('Subject %s dimension is %s' % (name, dim))
                        #print('Distance to %s is %s' % (name, distance))
                        #print('Scale factor is %s' % scale)
                        #print('Changing %s location to %s' % (name, object.location))
                elif token == 'sun_position' and len(elements) == 4:
                    name = 'Sun'
                    object = context.scene.objects.get(name, None)
                    if object:
                        location = [float(x) for x in elements[1:]]
                        distance = math.sqrt(location[0] * location[0] + location[1] * location[1] + location[2] * location[2])
                        scene_loc = object.location
                        scene_distance = math.sqrt(
                            scene_loc[0] * scene_loc[0] + scene_loc[1] * scene_loc[1] + scene_loc[2] * scene_loc[2])
                        scale =  scene_distance / distance
                        object.location = [x * scale for x in location]
                        object.keyframe_insert(data_path='location')
                        #print('Subject %s dimension is %s' % (name, dim))
                        #print('Distance to %s is %s' % (name, distance))
                        #print('Scale factor is %s' % scale)
                        #print('Changing %s location to %s' % (name, object.location))
                elif token == '':
                    if animate_frame:
                        animate_frame = False
                        finalize_frame_colors(context, object_temperatures, stats, use_global_temperatures, min_temperature, max_temperature,
                                              temperature_transform, animate_temperature, animate_color)

        if object_temperatures:
            context.scene.frame_set(current_keyframe + 1)
            finalize_frame_colors(context, object_temperatures, stats, use_global_temperatures, min_temperature,
                                  max_temperature, temperature_transform, animate_temperature, animate_color)
        context.scene.frame_set(0)
    return {'FINISHED'}


def linear(temp):
    return temp


def logarithmic(temp):
    return math.log10(temp)


def temperature_color(min_temp, max_temp, temp):
    hue = 0.67 * (1 - (temp - min_temp) / (max_temp - min_temp))
    c = Color()
    c.hsv = hue, 1, 0.5
    return c


def finalize_frame_colors(context, object_temperatures, stats, use_global_temperatures, min_temperature,
                              max_temperature, temperature_transform, animate_temperature, animate_color):
    objects = context.scene.objects
    min_temperature = temperature_transform(stats.min_temperature if use_global_temperatures else min_temperature)
    max_temperature = temperature_transform(stats.max_temperature if use_global_temperatures else max_temperature)
    for name, temperature in object_temperatures.items():
        transformed = temperature_transform(temperature)
        material_name = "%s.Temperature" % name
        material = bpy.data.materials.get(material_name, None)
        if not material:
            material = bpy.data.materials.new(material_name)
            material.emit = 0.4
        color = temperature_color(min_temperature, max_temperature, transformed)
        material.diffuse_color = color
        if animate_color:
            material.keyframe_insert(data_path='diffuse_color')

        subject = objects[name]
        subject.active_material = material

        subject.thorium.temperature = temperature
        if animate_temperature:
            subject.keyframe_insert(data_path='thorium.temperature')


class TaskStats(object):
    min_temperature = 1000000
    max_temperature = 0
    frames_count = 0
    lines_count = 0


def task_stats(filename):
    stats = TaskStats()

    with open(filename) as task:
        for line in task.readlines():
            stats.lines_count += 1
            elements = line.rstrip('\n').split(' ')
            token = elements[0]
            if token == 'tmprt':
                temperature = float(elements[1])
                stats.min_temperature = min(stats.min_temperature, temperature)
                stats.max_temperature = max(stats.max_temperature, temperature)
            elif token == 'newfrm':
                stats.frames_count += 1
    print('Min temp: %f, max temp: %f' % (stats.min_temperature, stats.max_temperature))
    return stats
